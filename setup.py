from setuptools import setup
setup(
    name="bpc_client",
    version="0.2",
    packages=['bpc_client'],
    scripts=['bin/bpc_json.py'],
)
