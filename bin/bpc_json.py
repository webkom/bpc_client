#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is a commandline script that does a query against BPC.
If all the correct arguments are supplied it prints the json to stdout.

The api-keys can be supplied either as arguments to the script or as
the environment variables BPC_FORENING and BPC_KEY.
"""

import argparse

from bpc_client.reguest_types import REQUEST_TYPES
from bpc_client.client import BPCClient


def parse_input():
    """Return the parameters needed for å bpc-request."""
    parser = argparse.ArgumentParser()
    parser.add_argument('request', choices=REQUEST_TYPES.keys())
    parser.add_argument('args', nargs=argparse.REMAINDER)
    parser.add_argument('--forening', type=int)
    parser.add_argument('--key')
    parser.add_argument('--testing', action='store_const',
                        default=False, const=True)

    return parser.parse_args()


def parse_request_parameters(request, args):
    """Parse the arguments required for the given request."""
    parser = argparse.ArgumentParser(prog=request)

    required = REQUEST_TYPES[request]['required']
    optional = REQUEST_TYPES[request]['optional']
    possible = required + optional

    for argument in required:
        parser.add_argument('--'+argument, required=True)
    for argument in optional:
        parser.add_argument('--'+argument, required=False)
    parsed = parser.parse_args(args)

    parameters = {p: getattr(parsed, p) for p in possible if getattr(parsed, p)}
    return parameters


if __name__ == '__main__':
    parsed = parse_input()
    client = BPCClient(forening=parsed.forening, key=parsed.key)
    client.testing = parsed.testing

    request = parsed.request
    parameters = parse_request_parameters(request, parsed.args)
    result = client.get_json_string(request=request, **parameters)
    print(result)
