# -*- coding: utf-8 -*-
from functools import partialmethod

from .client import DefaultClientFactory
from .conversions import convert_value, convert_datetime_arguments
from .exceptions import BPCResponseException, BPCConnectionError
import logging


BPC_NO_EVENTS = "403"
BPC_NO_USERS = "404"


class BPCEvent(object):
    """A class representing an event at BPC.

    Attributes are added dynamically from the data downloaded from BPC.
    """

    def __init__(self, bpc_id):
        self.client = DefaultClientFactory.get_instance()
        self.bpc_id = bpc_id
        self._data_downloaded = False
        self._data = None

    @classmethod
    def from_data(cls, data):
        """Create a BPCEvent-object from dictionary containing BPC-data."""
        bpc_id = data["id"]
        event = cls(bpc_id)
        event.data = data
        return event

    @property
    def data(self):
        if not self._data_downloaded:
            self.update_data()
        return self._data

    @data.setter
    def data(self, data):
        self._data = data
        self._data_downloaded = True

    def update_data(self):
        self.data = self.client.get_events(event=self.bpc_id)["event"][0]

    def __getattr__(self, name):
        try:
            return convert_value(name, self.data[name])
        except KeyError:
            raise AttributeError("Unknown attribute {}".format(name))

    def __repr__(self):
        return "<BPCEvent: {0.bpc_id}:{0.title}>".format(self)

    def _get_user_list(self, list_type):
        method = self.client.__getattribute__(list_type)
        try:
            result = method(event=self.bpc_id)
            return result["users"]
        except BPCResponseException as e:
            if e.bpc_error_code == BPC_NO_USERS:
                return []
            else:
                raise e

    get_attending = partialmethod(_get_user_list, list_type="get_attending")
    get_waiting = partialmethod(_get_user_list, list_type="get_waiting")

    def add_attending(self, username, fullname, card_no, year):
        """Tries to register the user.

        Returns: True if attending and False if waiting
        Throws BPCResponseException if it doesn't work.

        Inputs:
            fullname: Full name of user
            username:
            card_no: SHA1-hash of NTNU-cardnumber
            year: Year of student (klassetrinn)
        """
        result = self.client.add_attending(
            event=self.bpc_id,
            username=username,
            fullname=fullname,
            card_no=card_no,
            year=year)
        return result["add_attending"][0] == "1"

    def rem_attending(self, username):
        """Tries to deregister a user."""
        self.client.rem_attending(event=self.bpc_id, username=username)

    @property
    def attending_usernames(self):
        return [user["username"] for user in self.get_attending()]

    @property
    def waiting_usernames(self):
        return [user["username"] for user in self.get_waiting()]


def get_events(**query):
    query = convert_datetime_arguments(query)
    client = DefaultClientFactory.get_instance()
    try:
        data = client.get_events(**query)
    except BPCResponseException as e:
        if e.bpc_error_code == BPC_NO_EVENTS:
            return []
        else:
            raise e
    except BPCConnectionError:
        logger = logging.getLogger(__name__)
        logger.error('bedriftpresentasjon.no svarer ikke')
        return []
    return [BPCEvent.from_data(x) for x in data['event']]
