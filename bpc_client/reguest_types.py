REQUEST_TYPES = {
    'get_events': {
        'required': (),
        'optional': ('event', 'username', 'fromdate', 'todate', 'event_type')
    },
    'add_attending': {
        'required': ('fullname', 'username', 'card_no', 'event', 'year'),
        'optional': ('user_id',)
    },
    'rem_attending': {
        'required': ('event', 'username'),
        'optional': ()
    },
    'get_attending': {
        'required': ('event',),
        'optional': ('sort',)
    },
    'get_waiting': {
        'required': ('event',),
        'optional': ('sort',)
    },
    'get_user_stats': {
        'required': ('username',),
        'optional': ('detailed_stats', 'fromdate', 'todate', 'event_type')
    },
    'get_event_stats': {
        'required': (),
        'optional': ('fromdate', 'todate', 'event_type', 'attended', 'show_waitlist')
    },
}
