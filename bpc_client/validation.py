from .reguest_types import REQUEST_TYPES
from .exceptions import BPCValidationError


def validate_request(request):
    """
    Validates a request to BPC.

    Raises an BPCValidationError on invalid request.
    """

    # Check if the type of the request is supplied.
    try:
        request_type = request['request']
    except KeyError:
        raise BPCValidationError('No request type supplied.')

    # Check if request type is of a known type.
    try:
        possible_parameters = REQUEST_TYPES[request_type]
    except KeyError:
        raise BPCValidationError('Unknown request type {}'
                                 .format(request_type))

    required = set(possible_parameters['required'])
    optional = set(possible_parameters['optional'])

    supplied = set(request) - {'request'}

    # Check if all the required parameters for the request is supplied.
    if not supplied.issuperset(required):
        missing = required - supplied
        raise BPCValidationError(
            'Missing required parameters {missing} for request-type {request_type}'
            .format(**locals()))

    # Check if the rest of the parameters are supported.
    supplied_not_required = supplied - required
    if not supplied_not_required.issubset(optional):
        unknown = supplied_not_required - optional
        raise BPCValidationError(
            'Unknown parameters {unknown} for request-type {request_type}'
            .format(**locals()))
