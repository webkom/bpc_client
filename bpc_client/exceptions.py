# -*- coding: utf-8 -*-
import logging
logger = logging.getLogger(__name__)


class BPCException(Exception):
    pass


class BPCConnectionError(BPCException):
    pass


class BPCValidationError(BPCException):
    pass


class BPCResponseException(BPCException):
    """Exceptions returned from BPC."""
    def __init__(self, response):
        self.response = response

    def __str__(self):
        return str(self.response)

    @property
    def bpc_error_code(self):
        return list(self.response['error'][0].keys())[0]

    @property
    def bpc_error_message(self):
        try:
            return BPC_ERRORS[self.bpc_error_code]
        except KeyError:
            logger.info("An unknown error-code, {}, was returned from BPC".format(self.bpc_error_code))
            return "Unknown error code. {}".format(self.bpc_error_code)



# Typer feil som BPC kan rapportere om.
# Tatt fra BPC-readme
BPC_ERRORS = {
    # Fatalt - Disse avbryter skriptet.
    '101': 'Feil eller ingen handshake',
    '102': 'Feil eller ingen request',
    '103': 'Ingen eller ikke-eksisterende event',
    '104': 'Feil i SQL-spørring',
    '105': 'Feil eller ikke noe brukernavn gitt',
    '106': 'Feil eller ikke noe fullstendig navn gitt',
    '107': 'Feil eller ikke noe kortnummer gitt',
    '108': 'Feil versjon av integrasjonskoden',
    '109': 'Handshake støtter ikke denne operasjonen',

    # Feil - Alvorlige feil som ikke bør skje med en korrekt oppsatt
    # klient, men ikke så alvorlige at skriptet avbrytes. En fatal
    # feil vil ofte følge etter en vanlig feil, disse kan da hjelpe
    # til å spore problemet.
    '201': 'Feil i validering av en parameter',
    '202': 'Du har ikke tilgang til å gjøre dette',
    '203': 'PHP-feil i scriptet på serveren',
    '204': 'Manglende JSONP-callbackparameter, kan ikke returnere data',

    # Tilbakemeldinger - Dette er vanlige tilbakemeldinger som kan
    # komme og som bør tas høyde for ved bruk av funksjonene gitt i
    # parantes.
    '401': 'Studenten går ikke på høyt nok årstrinn for dette arrangementet',
    '402': 'Det finnes ikke ledige plasser på dette arrangementet',
    '403': 'Det finnes ingen arrangementer som passer med denne forespørselen',
    '404': 'Det finnes ingen deltagere som passer med denne forespørselen',
    '405': 'Personen (kortnummer eller brukernavn) er allerede påmeldt dette arrangementet',
    '406': 'Brukernavnet er ikke påmeldt dette arrangementet og kan dermed ikke meldes av',
    '407': 'Brukeren var ikke påmeldt som medlem av denne linjeforeningen, og kan dermed ikke meldes av',
    '408': 'Påmelding har ikke startet ennå',
    '409': 'Påmeldingsfristen har passert',
    '410': 'Studenten går i et for høyt årstrinn for dette arrangementet.',
    }
