import os
import json
from urllib.parse import urlencode
from urllib.request import urlopen
from urllib.error import URLError
from functools import partialmethod

from .validation import validate_request
from .exceptions import BPCResponseException, BPCConnectionError


class BPCClient(object):
    BPC_URL = 'https://www.bedriftspresentasjon.no/remote/'
    BPC_TESTING_URL = 'http://testing.bedriftspresentasjon.no/remote/'
    VERSION = '1.6'
    validate = True
    raise_exceptions = True

    def __init__(self, forening, key, testing=False):
        self.forening = forening
        self.key = key
        self.testing = testing

    @classmethod
    def from_config(cls, config):
        forening = config.get("BPC_FORENING", 0)
        key = config.get("BPC_KEY", "")
        testing = config.get("BPC_TESTING", False)
        return cls(forening=forening, key=key, testing=testing)

    @property
    def bpc_url(self):
        return self.BPC_TESTING_URL if self.testing else self.BPC_URL

    def raw_request(self, **data):
        """Returns a file-like object of bytes with the response from BPC.

        The keyword arguments are the arguments in the BPC-query.
        """
        encoded_data = urlencode(data).encode()
        try:
            return urlopen(self.bpc_url, data=encoded_data, timeout=1)
        except URLError:
            raise BPCConnectionError("Can't get a connection to BPC-server at {url}".format(url=self.bpc_url))

    def get_json_string(self, **data):
        return self.raw_request(
            forening=self.forening,
            key=self.key,
            method='json',
            version=self.VERSION,
            **data).read().decode("utf-8")

    def query(self, **data):
        """Returns the response as a dictionary."""
        if self.validate:
            validate_request(data)
        json_object = json.loads(self.get_json_string(**data))
        if "error" in json_object and self.raise_exceptions:
            raise BPCResponseException(response=json_object)
        return json_object

    get_events = partialmethod(query, request="get_events")
    add_attending = partialmethod(query, request="add_attending")
    rem_attending = partialmethod(query, request="rem_attending")
    get_attending = partialmethod(query, request="get_attending")
    get_waiting = partialmethod(query, request="get_waiting")
    get_user_stats = partialmethod(query, request="get_user_stats")
    get_event_stats = partialmethod(query, request="get_event_stats")


class DefaultClientFactory(object):
    default_client = None

    @classmethod
    def get_instance(cls):
        if cls.default_client is None:
            cls.configure(os.environ)
        return cls.default_client

    @classmethod
    def configure(cls, config):
        cls.default_client = BPCClient.from_config(config)
