from datetime import datetime

# Date format used by BPC. ISO8601
BPC_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

event_conversion = {
    'time': 'datetime',
    'deadline': 'datetime',
    'deadline_passed': 'bool',
    'is_advertised': 'bool',
    'registration_start': 'datetime',
    'registration_started': 'bool',
    'seats': 'int',
    'seats_available': 'int',
    'this_attending': 'int',
    'waitlist_enabled': 'bool',
    'count_waiting': 'int',
    # Kun hvis username er gitt i forespørsel
    'is_waiting': 'bool',
    'attending': 'bool',
    }

datetime_arguments = ['todate', 'fromdate']


def bpc_time_to_datetime(bpc_time):
    return datetime.strptime(bpc_time, BPC_TIME_FORMAT)


def datetime_to_bpc_time(dt):
    return dt.strftime(BPC_TIME_FORMAT)


def convert_datetime_arguments(mapping):
    return {
        key: (
            datetime_to_bpc_time(value)
            if (key in datetime_arguments and isinstance(value, datetime))
            else value
        )
        for key, value in mapping.items()
    }


def convert_value(name, value):
    attribute_type = event_conversion.get(name, 'string')
    if attribute_type == 'string':
        return str(value)
    elif attribute_type == 'bool':
        return bool(int(value))
    elif attribute_type == 'int':
        return int(value)
    elif attribute_type == 'datetime':
        return bpc_time_to_datetime(value)
