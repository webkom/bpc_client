import unittest
from itertools import combinations, chain

from bpc_client.validation import validate_request
from bpc_client.exceptions import BPCValidationError
from bpc_client.reguest_types import REQUEST_TYPES


def powerset(l, proper=False):
    """Gets an iterator over all subsets of l"""
    max_length = len(l) if proper else len(l)+1
    return chain.from_iterable(combinations(l, n) for n in range(max_length))


def get_dummy_request(request_type, parameters):
    """Creates a request with dummy parameters."""
    request = {x: "some_value" for x in parameters}
    request['request'] = request_type
    return request


class RequestValidationTest(unittest.TestCase):
    """Tests the validation of request to the BPC-server."""

    def test_no_request_type(self):
        with self.assertRaises(BPCValidationError) as cm:
            validate_request({})

        the_exception = cm.exception
        self.assertIn('No request type', the_exception.args[0])

    def test_unknown_request_type(self):
        request_type = 'invalid_request'
        with self.assertRaises(BPCValidationError) as cm:
            validate_request({'request': request_type})

        the_exception = cm.exception
        self.assertIn('Unknown request type', the_exception.args[0])
        self.assertIn(request_type, the_exception.args[0])

    def test_valid_requests_are_valid(self):
        for request_type, params in REQUEST_TYPES.items():
            required = frozenset(params['required'])
            optional = frozenset(params['optional'])

            # Go through all combinations of optional parameters and check if
            # the request is valid
            for opt_comb in powerset(optional):
                parameters = set(opt_comb) | required
                request = get_dummy_request(request_type, parameters)
                validate_request(request)

    def test_missing_required_parameters(self):
        for request_type, params in REQUEST_TYPES.items():
            required = frozenset(params['required'])

            # Go through all combinations of missing required parameters,
            # and test if request is invalid.
            for required_comb in powerset(required, proper=True):
                request = get_dummy_request(request_type, required_comb)
                with self.assertRaises(
                        BPCValidationError,
                        msg='Request was: {request}'.format(**locals())):
                    validate_request(request)

    def test_unknown_parameters(self):
        invalid_parameters = {'nooooooo', 'asdf', 'sdf46'}
        for request_type, params in REQUEST_TYPES.items():
            required = frozenset(params['required'])
            parameters = required | invalid_parameters
            request = get_dummy_request(request_type, parameters)

            with self.assertRaises(
                    BPCValidationError,
                    msg="Unknown parameters were validated. Request: {}".
                    format(request)):
                validate_request(request)
