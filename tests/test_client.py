import unittest
from unittest.mock import patch
import io

from bpc_client.client import BPCClient
from bpc_client.exceptions import BPCResponseException, BPCConnectionError


class ClientTest(unittest.TestCase):
    def setUp(self):
        self.forening = "1"
        self.key = ""
        self.client = BPCClient(
            forening=self.forening,
            key=self.key,
            testing=True)


class RawRequestTest(ClientTest):

    @patch("bpc_client.client.urlopen")
    @patch("bpc_client.client.urlencode")
    def test_raw_request(self, mock_urlencode, mock_urlopen):
        response = self.client.raw_request(some_parameter="hey", another_parameter="hoi")
        self.assertEqual(mock_urlencode.call_count, 1)
        self.assertEqual(mock_urlopen.call_count, 1)
        self.assertEqual(response, mock_urlopen.return_value)

    def test_cant_connect_to_server(self):
        self.client.BPC_TESTING_URL = "http://thisisnota.url"
        self.client.BPC_URL = "http://thisisnota.url"
        self.assertRaises(BPCConnectionError, self.client.raw_request)


@patch("bpc_client.client.BPCClient.raw_request")
class QueryTest(ClientTest):

    @patch("bpc_client.client.validate_request")
    def test_query_is_validated(self, mock_validate_request, mock_raw_request):
        mock_raw_request.return_value = io.BytesIO(b'{}')
        json_dict = self.client.query()
        self.assertEqual(mock_validate_request.call_count, 1)
        self.assertIsInstance(json_dict, dict)

    @patch("bpc_client.client.validate_request")
    def test_query_ignores_validation(self, mock_validate_request, mock_raw_request):
        mock_raw_request.return_value = io.BytesIO(b'{}')
        self.client.validate = False
        self.client.query(request="not_a_request")
        self.assertEqual(mock_validate_request.call_count, 0)

    def test_raises_exception_on_non_json_response(self, mock_raw_request):
        mock_raw_request.return_value = io.BytesIO(b'this is not json')
        self.assertRaises(ValueError, self.client.query, request="get_events")

    def test_raises_response_exception_on_error_message_from_bpc(self, mock_raw_request):
        mock_raw_request.return_value = io.BytesIO(
            b'{"error":[{"102": "Fatal: No request given."}]}')
        with self.assertRaises(BPCResponseException) as ctx:
            self.client.query(request="get_events")
        e = ctx.exception
        self.assertEqual(e.bpc_error_code, "102")
