import unittest

from bpc_client.exceptions import BPCResponseException


class TestBPCResponseException(unittest.TestCase):

    def setUp(self):
        self.error_code = "101"
        self.response = {"error": [{self.error_code: "Error message"}]}
        self.e = BPCResponseException(response=self.response)

    def test_bpc_error_code(self):
        self.assertEqual(self.error_code, self.e.bpc_error_code)

    def test_bpc_error_message(self):
        s = self.e.bpc_error_message
        self.assertIsInstance(s, str)

    def test_unknown_bpc_error_message(self):
        e = BPCResponseException({"error": [{"100000000": "Error message"}]})
        self.assertIn("Unknown", e.bpc_error_message)

    def test_str(self):
        s = str(self.e)
        self.assertIn(self.e.bpc_error_code, s)
