import unittest
from datetime import datetime

from bpc_client.conversions import convert_value, event_conversion, convert_datetime_arguments


class TestConvertValue(unittest.TestCase):

    def test_string(self):
        """Unknown attributes stays as strings"""
        strings = ["not_known", "just", "some", "test", "attributes"]
        for s in strings:
            return_value = convert_value(s, s)
            self.assertEqual(s, return_value)
            self.assertIsInstance(return_value, str)

    def test_numbers(self):
        number_attributes = [key for key, value in event_conversion.items() if value == "int"]
        for s in number_attributes:
            return_value = convert_value(s, "3")
            self.assertIsInstance(return_value, int)

    def test_datetimes(self):
        datetime_attributes = [key for key, value in event_conversion.items() if value == "datetime"]
        time = '2015-05-15 15:04:51'
        for s in datetime_attributes:
            return_value = convert_value(s, time)
            self.assertIsInstance(return_value, datetime)


class TestDatetimeConversions(unittest.TestCase):
    def test_datetime_arguments(self):
        query = {
            'fromdate': datetime(2016, 4, 1),
            'todate': datetime(2016, 5, 1)
        }
        converted = convert_datetime_arguments(query)
        self.assertIsInstance(converted['fromdate'], str)
        self.assertIsInstance(converted['todate'], str)
