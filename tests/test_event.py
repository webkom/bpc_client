# -*- coding: utf-8 -*-

import unittest
from unittest.mock import patch
from datetime import datetime
from bpc_client.event import BPCEvent, get_events
from bpc_client.exceptions import BPCResponseException

one_json_event = {"event": [{
    "count_waiting": 0,
    "deadline": "2019-10-11 17:00:00",
    "deadline_passed": "0",
    "description": "Dette er en kjempebra bedrift.",
    "description_formatted": "<p>Dette er en kjempebra bedrift.</p>\n",
    "id": "10",
    "is_advertised": "1",
    "max_year": "3",
    "min_year": "3",
    "open_for": "3",
    "place": "kontoret",
    "registration_start": "2019-10-11 11:00:00",
    "registration_started": "0",
    "seats": "10",
    "seats_available": "10",
    "this_attending": "0",
    "time": "2023-10-12 17:00:00",
    "title": "jesus",
    "waitlist_enabled": "1",
    "web_page": "http://jesus.com"
    }]
}


class BPCEventTest(unittest.TestCase):

    def setUp(self):
        self.event_id = 10
        self.event = BPCEvent(self.event_id)
        self.users = [{"username": "username%s" % i,
                       "fullname": "Full name",
                       "card_no": "shashasha",
                       "year": str(i)}
                      for i in range(1, 6)]

    def test_repr(self):
        event = BPCEvent.from_data(one_json_event["event"][0])
        s = str(event)
        self.assertIn("BPCEvent", s)

    @patch("bpc_client.client.BPCClient.query")
    def test_create_event_without_contacting_bpc(self, mock_method):
        self.assertEqual(mock_method.call_count, 0)

    @patch("bpc_client.client.BPCClient.get_events")
    def test_get_data(self, mock_get_events):
        mock_get_events.return_value = one_json_event

        self.assertEqual(self.event_id, self.event.bpc_id)

        data = self.event.data
        mock_get_events.assert_called_with(event=self.event_id)
        self.assertIs(data, one_json_event["event"][0])

    @patch("bpc_client.client.BPCClient.get_events")
    def test_access_data_as_attribute_of_event(self, mock_get_events):
        mock_get_events.return_value = one_json_event
        time = self.event.time
        self.assertEqual(time, datetime(2023, 10, 12, 17, 0, 0))

        registration_started = self.event.registration_started
        self.assertEqual(registration_started, False)
        self.assertIsInstance(registration_started, bool)

    @patch("bpc_client.client.BPCClient.get_events")
    def test_create_from_data(self, mock_get_events):
        event_data = one_json_event["event"][0]
        event = BPCEvent.from_data(event_data)
        self.assertEqual(event.data, event_data)
        self.assertFalse(mock_get_events.called)

    def test_get_unknown_attribute(self):
        event = BPCEvent.from_data({"id": 10})
        with self.assertRaises(AttributeError):
            event.not_an_attribute

    @patch("bpc_client.client.BPCClient.get_attending")
    def test_get_attending(self, mock_get_attending):
        mock_get_attending.return_value = {"users": self.users}
        attending_users = self.event.get_attending()
        self.assertEqual(attending_users, self.users)

    @patch("bpc_client.client.BPCClient.get_attending")
    def test_no_users_attending(self, mock_get_attending):
        mock_get_attending.side_effect = BPCResponseException(
            {"error": [{"404": "Error: No users attending."}]}
        )
        attending_users = self.event.get_attending()
        self.assertEqual(attending_users, [])

    @patch("bpc_client.client.BPCClient.get_attending")
    def test_error_when_getting_attending_users(self, mock_get_attending):
        mock_get_attending.side_effect = BPCResponseException(
            {"error": [{"202": "Error: No access."}]}
        )
        with self.assertRaises(BPCResponseException):
            self.event.get_attending()

    @patch("bpc_client.client.BPCClient.get_waiting")
    def test_get_waiting(self, mock_get_waiting):
        mock_get_waiting.return_value = {"users": self.users}
        waiting_users = self.event.get_waiting()
        self.assertEqual(waiting_users, self.users)

    @patch("bpc_client.client.BPCClient.get_waiting")
    def test_no_users_waiting(self, mock_get_waiting):
        mock_get_waiting.side_effect = BPCResponseException(
            {"error": [{"404": "Error: No users on waitlist."}]}
        )
        waiting_users = self.event.get_waiting()
        self.assertEqual(waiting_users, [])

    @patch("bpc_client.client.BPCClient.add_attending")
    def test_add_attending(self, mock_add_attending):
        # Test added to attending list
        mock_add_attending.return_value = {"add_attending": ["1"]}
        is_attending = self.event.add_attending(**self.users[0])
        self.assertTrue(is_attending)
        mock_add_attending.assert_called_with(event=self.event_id, **self.users[0])

        # Test added to waiting list
        mock_add_attending.return_value = {"add_attending": ["0"]}
        is_attending = self.event.add_attending(**self.users[0])
        self.assertFalse(is_attending)

    @patch("bpc_client.event.BPCEvent.get_attending")
    def test_attending_usernames(self, mock_get_attending):
        mock_get_attending.return_value = self.users[::2]
        usernames = self.event.attending_usernames
        self.assertEqual(len(mock_get_attending.return_value), len(usernames))

    @patch("bpc_client.event.BPCEvent.get_waiting")
    def test_waiting_usernames(self, mock_get_waiting):
        mock_get_waiting.return_value = self.users[1::2]
        usernames = self.event.waiting_usernames
        self.assertEqual(len(mock_get_waiting.return_value), len(usernames))

    @patch("bpc_client.client.BPCClient.rem_attending")
    def test_rem_attending(self, mock_rem_attending):
        username = "username"
        self.event.rem_attending(username=username)

    @patch("bpc_client.client.BPCClient.add_attending")
    def test_add_utf8_name(self, mock_add_attending):
        user = self.users[0]
        user["fullname"] = u"Den Ærefulle øltørste Ålen"
        self.event.add_attending(**user)


class GetEventsTest(unittest.TestCase):

    @patch("bpc_client.client.BPCClient.get_events")
    def test_all_events(self, mock_get_events):
        mock_get_events.return_value = {"event": [{"id": "1"}, {"id": "12"}, {"id": "43"}]}
        events = get_events()
        for event in events:
            self.assertIsInstance(event, BPCEvent)
